require 'test_helper'

class Brick
  extend Lyon::Configuration
  
  define_setting :access_token  
  define_setting :access_secret
  define_setting :signature, 'x10'
    
  define_setting :favorite_liquid, "apple juice"
end

class LyonTest < Minitest::Test

  def test_can_access_configuration_value_that_has_value
    Brick.configuration do |config|
      config.access_token = 'token'
    end

    assert_equal Brick.access_token, 'token'
  end
  
  def test_configuration_value_that_is_not_set_is_nil    
    assert Brick.access_secret.nil?
  end
  
  def test_can_override_the_default_value
    Brick.configuration do |config|
      config.favorite_liquid = "gluten free apple juice"
    end
    
    assert_equal Brick.favorite_liquid, 'gluten free apple juice'
  end
  
  def test_can_access_configuration_value_with_default_value
    assert_equal Brick.signature, 'x10'
  end
end
