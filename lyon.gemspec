# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'lyon/version'

Gem::Specification.new do |spec|
  spec.name          = "lyon"
  spec.version       = Lyon::VERSION
  spec.authors       = ["Bala Paranj"]
  spec.email         = ["bparanj@gmail.com"]

  spec.summary       = %q{Easy Gem Configuration Variables with Defaults.}
  spec.description   = %q{A simple gem configuration gem to allow a gem user to configure gem specific variables.}
  spec.homepage      = "https://viget.com/extend/easy-gem-configuration-variables-with-defaults"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.12.5"
  spec.add_development_dependency "rake", "~> 10.5"
  spec.add_development_dependency "minitest", "~> 5.8"
  spec.add_development_dependency "simplecov", "~> 0.11.2"
  spec.add_development_dependency "rubycritic"
end
