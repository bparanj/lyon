# Lyon

To experiment with that code, run `bin/console` for an interactive prompt. I have packaged the source code found in the blog post by Eli Fatsi : https://viget.com/extend/easy-gem-configuration-variables-with-defaults into a gem.

## Version

Ruby : 2.3.1

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'lyon'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install lyon

## Usage

In your my_gem/lib/my_gem.rb:

```ruby
module MyGem
  extend Lyon::Configuration

  define_setting :access_token
  define_setting :access_secret
  
  define_setting :favorite_liquid,       "apple juice"
  define_setting :least_favorite_liquid, "seltzer water"
end
```

In your Rails app, config/initializers/my_gem.rb:

```ruby
MyGem.configuration do |config|
  config.access_token  = "token"
  
  config.favorite_liquid = "gluten free apple juice"
end
```

With this configuration set, you’ve got the following variables available to use within your gem and the app using the gem:

```ruby
MyGem.access_token  #=> "token"
MyGem.access_secret #=> nil (was never assigned so remains nil)

MyGem.favorite_liquid       #=> "gluten free apple juice"
MyGem.least_favorite_liquid #=> "seltzer water"
```

You can see the default value favorite_liquid (apple juice) is over-ridden to 'gluten free apple juice'. Read the tests to learn how this works.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on bitbucket at https://bitbucket.org/bparanj/lyon.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

